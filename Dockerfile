FROM golang:1.13

RUN mkdir -p /go/src/tsf-prometheus-exporter

WORKDIR /go/src/tsf-prometheus-exporter

COPY . /go/src/tsf-prometheus-exporter/
ENV GO111MODULE on
RUN go build -mod=vendor -o main main.go

EXPOSE 8081

CMD /go/src/tsf-prometheus-exporter/main
