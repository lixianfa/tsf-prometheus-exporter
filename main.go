package main

import (
	"flag"
	"log"
	"net/http"

	"bitbucket.org/mathildetech/tsf-prometheus-exporter/collector"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
	listenAddr             = flag.String("port", "9004", "An port to listen on for web interface and telemetry.")
	metricsName            = flag.String("name", "product_metric", "Prometheus metrics name")
	metricsAppName         = flag.String("app", "tsfproducts", "Prometheus metrics name")
	metricsAppGuageName    = flag.String("applabel", "application", "Prometheus metrics name")
	metricsSvcGuageName    = flag.String("svclabel", "service", "Prometheus metrics name")
	metricsLabelProductKey = flag.String("pkey", "product", "Prometheus metrics name")
	metricsLabelVersionKey = flag.String("vkey", "version", "Prometheus metrics name")
)

var (
	appVersion = "v1.12.4"
)

func main() {
	flag.Parse()

	metricPath := "/metrics"

	metrics := collector.NewMetrics(*metricsName, *metricsAppName, *metricsAppGuageName, *metricsSvcGuageName, appVersion,
		*metricsLabelProductKey, *metricsLabelVersionKey)
	registry := prometheus.NewRegistry()
	registry.MustRegister(metrics)

	http.Handle(metricPath, promhttp.HandlerFor(registry, promhttp.HandlerOpts{}))
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`<html>
			<head><title>A Prometheus Exporter</title></head>
			<body>
			<h1>A Prometheus Exporter</h1>
			<p><a href='/metrics'>Metrics</a></p>
			</body>
			</html>`))
	})

	log.Printf("Starting Server at http://localhost:%s%s", *listenAddr, metricPath)
	log.Fatal(http.ListenAndServe(":"+*listenAddr, nil))
}
