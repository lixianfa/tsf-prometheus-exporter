package config

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"os"
)

func init() {
	viper.SetDefault("LOG_LEVEL", "DEBUG")
	viper.SetDefault("ACP_ADAPTER_ENDPOINT", "http://localhost:8081")
	viper.SetDefault("TSF_METRICS_ROUTE", "/sync/capacity")
	viper.AutomaticEnv()
	configLogger()
}

var Logger = logrus.New()

func configLogger() {
	level := viper.GetString("LOG_LEVEL")
	switch level {
	case "DEBUG":
		Logger.SetLevel(logrus.DebugLevel)
	case "INFO":
		Logger.SetLevel(logrus.InfoLevel)
	default:
		Logger.SetLevel(logrus.InfoLevel)
	}

	Logger.SetOutput(os.Stdout)
	customFormatter := new(logrus.TextFormatter)
	customFormatter.TimestampFormat = "2006-01-02 15:04:05"
	customFormatter.FullTimestamp = true
	customFormatter.DisableColors = true
	Logger.SetFormatter(customFormatter)
}
