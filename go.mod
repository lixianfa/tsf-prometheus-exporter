module bitbucket.org/mathildetech/tsf-prometheus-exporter

go 1.13

require (
	github.com/prometheus/client_golang v1.5.0
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/viper v1.6.2
	k8s.io/apimachinery v0.17.3 // indirect
)
