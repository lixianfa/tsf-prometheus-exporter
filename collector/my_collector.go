package collector

import (
	"bitbucket.org/mathildetech/tsf-prometheus-exporter/config"
	_ "bitbucket.org/mathildetech/tsf-prometheus-exporter/config"
	"fmt"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/spf13/viper"
	"sync"
)

// 指标结构体
type Metrics struct {
	metricName         string
	metricAppName      string
	metricAppGuageName string
	metricSvcGuageName string
	AppVersion         string
	metrics            map[string]*prometheus.Desc
	mutex              sync.Mutex
}

const MetricKey = "metricname"

func newGlobalMetric(metricName string, docString string, labels []string) *prometheus.Desc {
	return prometheus.NewDesc(metricName, docString, labels, nil)
}

func NewMetrics(name, appname, appguagename, svcguagename, appversion, prkey, vkey string) *Metrics {
	return &Metrics{
		metrics: map[string]*prometheus.Desc{
			name: newGlobalMetric(name, fmt.Sprintf("The description of %s", name), []string{prkey, vkey, MetricKey}),
		},
		metricName:         name,
		metricAppName:      appname,
		metricAppGuageName: appguagename,
		metricSvcGuageName: svcguagename,
		AppVersion:         appversion,
	}
}

func (c *Metrics) Describe(ch chan<- *prometheus.Desc) {
	for _, m := range c.metrics {
		ch <- m
	}
}

func (c *Metrics) Collect(ch chan<- prometheus.Metric) {
	c.mutex.Lock()
	defer c.mutex.Unlock()

	mockGaugeMetricData := c.GenerateMockData()
	for host, currentValue := range mockGaugeMetricData {
		ch <- prometheus.MustNewConstMetric(c.metrics[c.metricName], prometheus.GaugeValue, float64(currentValue), c.metricAppName, c.AppVersion, host)
	}
}

func (c *Metrics) GenerateMockData() (mockGaugeMetricData map[string]int) {
	// get tsf product metrics
	config.Logger.Debugf("ACP_ADAPTER_ENDPOINT: %s, TSF_METRICS_ROUTE: %s", viper.GetString("ACP_ADAPTER_ENDPOINT"), viper.GetString("TSF_METRICS_ROUTE"))
	ret := getRequest(viper.GetString("ACP_ADAPTER_ENDPOINT"), viper.GetString("TSF_METRICS_ROUTE"))
	mockGaugeMetricData = map[string]int{
		fmt.Sprintf("%s", c.metricAppGuageName): ret.Result.ApplicationCount,
		fmt.Sprintf("%s", c.metricSvcGuageName): ret.Result.RunMicroserviceCount,
	}
	return
}
