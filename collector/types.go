package collector

type TsfProductMetrics struct {
	ApplicationCount     int `json:"ApplicationCount"`
	RunMicroserviceCount int `json:"RunMicroserviceCount"`
}

type TsfResult struct {
	Result TsfProductMetrics `json:"result"`
}
