package collector

import (
	"bitbucket.org/mathildetech/tsf-prometheus-exporter/config"
	"encoding/json"
	"io/ioutil"
	"net/http"
)

func getRequest(url, routePath string) (result TsfResult) {
	//get请求
	//http.Get的参数必须是带http://协议头的完整url,不然请求结果为空
	resp, err := http.Get(url + routePath)
	if err != nil {
		config.Logger.Debugf("http Request: %s", err.Error())
		return
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		config.Logger.Debugf("getRequest body: %s", err.Error())
		return
	}

	err = json.Unmarshal(body, &result)
	if err != nil {
		config.Logger.Debugf("data Unmarshal: %s", err.Error())
		return
	}
	return
}
